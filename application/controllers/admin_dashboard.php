<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_user');
        $this->load->library('pagination');
        $this->load->helper('form');
    }

        public function index()
    {
       if( $this->session->userdata('login')==true){

        $this->load->view('admin/index');
       } else {
        redirect('admin_login');

       }
        
    }

public function formInsert(){
        if( $this->session->userdata('login')==true){
        $this->load->view('admin/form_input_user');
        } else {
        redirect('admin_login');

        }
}

public function insertUser(){

        if( $this->session->userdata('login')==true){
//set validation rules
        $this->form_validation->set_rules('nama', 'Nama', 'required|alpha|min_length[3]|max_length[40]|xss_clean');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');

        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');

        $this->form_validation->set_rules('role_id', 'Role ID', 'trim|required|max_length[1]|xss_clean');

        
        //validate form input
        if ($this->form_validation->run() == FALSE)
        {
            
        $this->load->view('admin/form_input_user');
        }
        else
        {


        switch ($this->input->post('role_id')){
                case 1 :
                $role = 'Super User';
                break;

                case 2 :
                $role = 'admin';
                break;

                default :
                $role = "";

            }

        $date=  date('Y-m-d  H:i:s');
        //insert the user registration details into database
        $data = array(
                'nama' => $this->input->post('nama'),
                'username' => $this->input->post('username'),
                'password' =>$this->input->post('password'),
                'hash_password' =>md5($this->input->post('password')),
                'role_id' => $this->input->post('role_id'),
                'role' => $role,
                'create_user' => $date,
            );

                 if ($this->m_user->insertUser($data))
                 {
                 echo "Insert data Berhasil";
                 echo "<meta http-equiv='refresh' content='2; url=".base_url()."Admin_dashboard/viewUser'>";
   
                 }else{

                 echo "Insert data Gagal";
                 echo "<meta http-equiv='refresh' content='2; url=".base_url()."Admin_dashboard/insertUser'>";
                }



            }

            } else {
            redirect('admin_login');

            }


            }

public function viewUser($id=NULL) {

        if( $this->session->userdata('login')==true){
        $jml = $this->db->get('user');

        //pengaturan pagination
        $config['base_url'] = base_url().'/admin_dashboard/viewUser';
        $config['total_rows'] = $jml->num_rows();
        $config['per_page'] = '5';

        $config['prev_link'] = '&laquo;';
        $config['next_link'] = '&raquo;';       
        
        //inisialisasi config
        $this->pagination->initialize($config);


        //buat pagination
        $data['halaman'] = $this->pagination->create_links();

        //tamplikan data
        $data['query'] = $this->m_user->getDataUser($config['per_page'], $id);
        $this->load->view('admin/show_table', $data);

        } else {
        redirect('admin_login');

       }
    }

    public function deleteUser($d) {

        if( $this->session->userdata('login')==true){
        $data['res'] = $this->m_user->deleteData(array('id' => $d));
                 if($data >= 1) {
                 echo "Delete Data Berhasil...!!!";
                 echo "<meta http-equiv='refresh' content='0; url=".base_url()."admin_dashboard/viewUser'>";
                 } else {
                 echo "Delete Data Gagal...!!!";
                 echo "<meta http-equiv='refresh' content='0; url=".base_url()."admin_dashboard/viewUser'>";
                 }

        } else {
        redirect('admin_login');

        }           
    }


public function updateUser($id){
if( $this->session->userdata('login')==true){
/*
    $this->form_validation->set_rules('nama', 'Nama', 'required|alpha|min_length[3]|max_length[40]|xss_clean');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');

        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');

        $this->form_validation->set_rules('role_id', 'Role ID', 'trim|required|max_length[1]|xss_clean');

        
        //validate form input
        if ($this->form_validation->run() == FALSE)
        {
            
       redirect('admin_dashboard/updateView/'.$id.'');
        } else { */
        $date= date('Y-m-d  H:i:s');
             switch ($this->input->post('role_id')){
                case 1 :
                $role = 'Super User';
                break;

                case 2 :
                $role = 'admin';
                break;

                default :
                $role = "";

             }
  
             $data=array('nama'=>$this->input->post('nama'),'username'=>$this->input->post('username'),'password'=>$this->input->post('password'),'hash_password'=>md5($this->input->post('password')),'role_id'=>$this->input->post('role_id'),'role'=>$role,'create_user'=>$date);

             $this->m_user->update($id,$data);

            
             echo "Insert data Berhasil";
             echo "<meta http-equiv='refresh' content='2; url=".base_url()."Admin_dashboard/viewUser'>";


     //   }
     


        } else {
        redirect('admin_login');

       }
}

public function updateView($id) {
    if( $this->session->userdata('login')==true){
      
             $data['user_data']=$this->m_user->editData($id);
             $this->load->view('admin/form_update',$data);
              

  } else {
        redirect('admin_login');

       }

                  
     }




    function logout() {
        $this->session->sess_destroy();
        redirect('admin_login');
    }













}

