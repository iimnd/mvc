<?php
class Admin_login extends CI_Controller {

    function __construct() {
        parent::__construct();
        session_start();
        $this->load->model(array('m_user'));
        if ($this->session->userdata('username')) {
        redirect('admin_dashboard');
        }
    }
    function index() {
        $this->load->view('login');
    }

    function Login_process() {
        $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {

            $usr = $this->input->post('username');
            $psw = $this->input->post('password');
            $u = mysql_real_escape_string($usr);
            $p = md5(mysql_real_escape_string($psw));
            $cek = $this->m_user->cek($u, $p);
            if ($cek->num_rows() > 0) {
                //insert last login
                $this->m_user->lastLogin($u, $p);
                //login berhasil, buat session
                foreach ($cek->result() as $qad) {
                    $sess_data['id'] = $qad->id;
                    $sess_data['nama'] = $qad->nama;
                    $sess_data['username'] = $qad->username;
                    $sess_data['role'] = $qad->role;
                    $sess_data['role_id'] = $qad->role_id;
                    $sess_data['image'] = $qad->image;
                    $sess_data['login'] =true;
                    $this->session->set_userdata($sess_data);
                }

                redirect('admin_dashboard');
            } else {
                $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
                redirect('admin_login');
            }
        }
    }

   
}
