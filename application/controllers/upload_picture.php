<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_picture extends CI_Controller {

    var $limit=10;
    var $offset=10;
//var  $path= realpath(APPPATH . '../uploads/users/');
    public function __construct() {
        parent::__construct();
        $this->load->model('m_user'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 
       
  $this->gallery_path = realpath(APPPATH . '../uploads/users');
  $this->gallery_path_url = base_url().'/uploads/users';
    }

    public function index($page=NULL,$offset='',$key=NULL)
    {
        $data['titel']='Upload CodeIgniter'; //varibel title
        
        $data['query'] = $this->m_user->get_allimage(); //query dari model
        
        $this->load->view('vupload',$data); //tampilan awal ketika controller upload di akses
    }
    public function add() {
        
        $data['titel']='Form Upload CodeIgniter'; //varibel title
         $data['query'] = $this->m_user->get_allimage();
        
        $this->load->view('fupload',$data);
       
    }

    public function checkFile($name){
      $ext = pathinfo($name, PATHINFO_EXTENSION);

    

    }
    public function insert($id){

        $this->load->library('upload');
        $nmfile = "file_user_id_".$id; //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = $this->gallery_path; //path folder
        $config['allowed_types'] = 'jpg'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '5048'; //maksimum besar file 2M
        $config['max_width']  = '5120'; //lebar maksimum 1288 px
        $config['max_height']  = '5120'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
       
      

        if($_FILES['filefoto']['name'])
        { 

        $name = $_FILES['filefoto']['name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        $file =$this->gallery_path.'\\'.$config['file_name'].'.'.$ext;
         if (is_file($file)){
             unlink($file);
          

        }
         
       
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                  $thumbs_config = array(
   'source_image' => $gbr['full_path'],
   'new_image' => $this->gallery_path.'/thumbs',
   'file_name'=>$gbr['file_name'],
   'maintain_ratio' => true,
   'width' => 150,
   'height' => 100,
  );
                  
     $this->load->library('image_lib', $thumbs_config);
  
     $this->image_lib->resize();
               
                $data = array(
                  'image' =>$gbr['file_name'],                 
                );

                $this->m_user->updatePicture($id,$data); //akses model untuk menyimpan ke database
                echo "sukses";die();
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Upload gambar berhasil !!</div></div>");
                redirect('upload'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                 echo "gagal";die();
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Gagal upload gambar !!</div></div>");
                redirect('upload/add'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }








}

