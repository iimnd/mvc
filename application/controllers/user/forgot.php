<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->database();
		$this->load->model('m_costumer');
	}
	
	function index()
	{
		$this->load->view('user/user_forgot');
	}

    function sendPassword()
    {
		//set validation rules
		
		$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email]');
		
		
		//validate form input
		if ($this->form_validation->run() == FALSE)
        {
			// fails
			$this->load->view('user/user_forgot');
        }
		else
		{
			$email =$this->input->post('email');
				
			$cek = $this->m_costumer->emailCheck($email);
		  foreach ($cek->result() as $qad) {
                    $myusername = $qad->username;
                      $mypassword = $qad->password;
                      $mynama = $qad->name;
                }
               
if ($this->m_costumer->sendPassword($email, $mypassword, $myusername,$mynama))
				{
					
					
				redirect('user/login');
				}
		
		}
	}
	
	function verify($hash=NULL)
	{
		if ($this->m_costumer->verifyEmailID($hash))
		{
			$this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
			redirect('user/register/register');
		}
		else
		{
			$this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
			redirect('user/register/register');
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */