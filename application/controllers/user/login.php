<?php
class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        session_start();
        $this->load->model(array('m_costumer'));
        if ($this->session->userdata('username')) {
            redirect('user/user_dashboard');
        }
    }
    function index() {
        $this->load->view('user/user_login');
    }

    function loginProcess() {
        $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('user/user_login');
        } else {

            $usr = $this->input->post('username');
            $psw = $this->input->post('password');
            $u = mysql_real_escape_string($usr);
            $p = md5(mysql_real_escape_string($psw));
            $cek = $this->m_costumer->costumerLogin($u, $p);
            if ($cek->num_rows() > 0) {
                //login berhasil, buat session
                foreach ($cek->result() as $qad) {
                    $sess_data['id'] = $qad->id;
                    $sess_data['nama'] = $qad->nama;
                    $sess_data['username'] = $qad->username;
                    $this->session->set_userdata($sess_data);
                }

             $data['username']= $sess_data['username'];
              
                redirect('user/user_dashboard', $data);
            } else {
                $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
                redirect('user/login');
            }
        }
    }

   
}
