<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->database();
		$this->load->model('M_costumer');
	}
	
	function index()
	{
		$this->register();
	}

    function register()
    {
		//set validation rules
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
		$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[M_costumer.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]');
		
		//validate form input
		if ($this->form_validation->run() == FALSE)
        {
			// fails
			$this->load->view('user/user_registration_view');
        }
		else
		{
			//insert the user registration details into database
			$data = array(
				'name' => $this->input->post('fname'),
				'last_name' => $this->input->post('lname'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'born' => $this->input->post('ttl'),
				'sex' => $this->input->post('gender'),
				'contact' => $this->input->post('contact'),
				'address' => $this->input->post('alamat'),
				'zip_code' => $this->input->post('kode_pos'),
				'hash' => md5($this->input->post('email')),
				'password' => $this->input->post('password'),
				'hashpassword' =>md5($this->input->post('password'))
			);
			
			// insert form data into database
			if ($this->M_costumer->insertUser($data))
			{
				// send email
				if ($this->M_costumer->sendEmail($this->input->post('email')))
				{
					// successfully sent mail
					$this->session->set_flashdata('msg','<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>');
				redirect('user/register/register');
				}
				else
				{
					// error
					$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
					redirect('user/register/register');
				}
			}
			else
			{
				// error
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('user/register/register');
			}
		}
	}
	
	function verify($hash=NULL)
	{
		if ($this->M_costumer->verifyEmailID($hash))
		{
			$this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
			redirect('user/register/register');
		}
		else
		{
			$this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
			redirect('user/register/register');
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */