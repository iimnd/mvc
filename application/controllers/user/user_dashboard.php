<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_dashboard extends CI_Controller {

	
	
	function index()
	{
		$this->load->view('user/user_dashboard');
	}

    
function logout() {
       
        $this->session->sess_destroy();
        redirect('user/login');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */