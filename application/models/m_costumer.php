<?php

class M_costumer extends CI_Model {

    private $table = "m_costumer";
    

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    


    function costumerLogin($username, $password) {


        $this->db->where("username", $username);
        $this->db->where("hashpassword", $password);
        $this->db->where("status", 1);
        return $this->db->get("m_costumer");
    }


 //insert into user table
    function insertUser($data)
    {
        return $this->db->insert('m_costumer', $data);
    }
    
    //send verification email to user's email id
    function sendEmail($to_email)
    {
        $from_email = 'your email';
        $subject = 'Verify Your Email Address';
        $message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br /> http://localhost:90/mvc/user/register/verify/' . md5($to_email) . '<br /><br /><br />Thanks<br />Mydomain Team';
        
        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = 'your password'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);
        
        //send mail
        $this->email->from($from_email, 'Mydomain');
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }
    

    function sendPassword($to_email, $password, $username,$nama)
    {
        $from_email = 'your email';
        $subject = 'Your Password';
        $message = 'Dear '.$nama.',<br /><br />This is your username and password info :<br /><br /> username : '.$username.' <br> password :  '.$password.' <br />Thanks<br />Mydomain Team';
        
        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = 'your password'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);
        
        //send mail
        $this->email->from($from_email, 'Mydomain');
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }
    //activate user account
    function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('m_costumer', $data);
    }
    function emailCheck($email) {
        $this->db->where("email", $email);
        
        return $this->db->get("m_costumer");
    }



}
