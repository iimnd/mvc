
<?php

class M_user extends CI_Model {

public function __construct() {
        parent::__construct();
    }

    public $TableName = "user";

    function cek($username, $password) {
        $this->db->where("username", $username);
        $this->db->where("hash_password", $password);
        return $this->db->get("user");
    }

function lastLogin($username, $password){
    $this->db->where("username", $username);
    $this->db->where("hash_password", $password);
    $date=  date('Y-m-d  H:i:s');
    $data=array('last_login' => $date);
    return $this->db->update("user", $data);

}
    
    public function insertUser($data) {

        $res = $this->db->insert('user', $data);
        return $res;
    }

    public function deleteData($where) {
            $res = $this->db->delete('user', $where);
            return $res;
    }

    function update($id,$data){
    
            $this->db->where('id',$id); //gunanya buat select where
            $this->db->update('user',$data); //gunanya buat update
           }


    public function updateData($data, $where) {
        $res = $this->db->update('user', $data, $where);
        return $res;
    }

    public function editData($id) {
        return $this->db->get_where('user',array('id'=>$id))->row(); 
    }

    public function getTotalRecord() {
        $query = $this->db->query("SELECT count(*) as Total FROM '.$TableName.'")->row_array();
        return $query['Total'];
    }
    public function getDataUser($num, $offset) {
        $this->db->order_by('id', 'ASC');
        $data = $this->db->get('user', $num, $offset);
        return $data->result();
    }

    public function getAllRecord() {
        $this->db->order_by('id', 'ASC');
        $data = $this->db->get();
        return $data->result();
    }


function getImage($id) {
       
        $query = $this->db->get_where('user',array('id'=>$id))->row(); 
        //cek apakah ada data
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function get_allimage() {
        $this->db->from('user');
        $query = $this->db->get();

        //cek apakah ada data
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
  
    function updatePicture($id,$data){
    $this->db->where("id", $id);
  

    return $this->db->update("user", $data);

}

}
