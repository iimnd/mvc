
<?php
$this->load->view('template/head');
?>
    <body class="skin-blue">
        <div class="wrapper">

            <?php
$this->load->view('template/header_message');
?>
                         <?php
$this->load->view('template/header_notif');
?>             
                                      <?php
$this->load->view('template/header_task');
?>    
                                     <?php
$this->load->view('template/header_user');
?>           
                           
            
                                     <?php
$this->load->view('template/left_menu');
?>           
   <!-- Right side column. Contains the navbar and content of the page -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Admin Dashboard
                        <small>Version 2.0</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- Info boxes -->
               
           
           <!-- you can start add content here --> 
   


          
                    <div class="row">
                        <div class="col-md-9">
                            <!-- TABLE: LATEST ORDERS -->
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">List User</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table class="table no-margin">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nama</th>
                                                    <th>Username</th>
                                                    <th>Role</th>
                                                     <th><center>Action Update</center></th>
                                                     <th><center>Action Delete</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             
                                                 <?php
          if (empty($query)) {
            echo 
              "<tr>
                <td colspan='6'><font color='red'>Not Record Count...!!!</font></td>
              </tr>";
          } else {
            $no = 1;
            foreach($query as $row) {
          ?>
            <tr>
              <td ><?php echo $row->id; ?></td>
              <td><?php echo $row->nama; ?></td>
              <td><?php echo $row->username; ?></td>
              <td><span class="label label-success"><?php echo $row->role; ?></span></td>
              <?php echo "<td style='text-align:center;'><a href='".base_url()."Admin_dashboard/updateView/".$row->id."'><button class='btn btn-default'><span class='glyphicon glyphicon-pencil'></span></button></a></td>"; ?>
              <?php echo "<td style='text-align:center;'><a href='".base_url()."Admin_dashboard/deleteUser/".$row->id."' type='button' class='btn btn-default'><span class='glyphicon glyphicon-trash'></span></a></td>"; ?>
            </tr>
          <?php
          $no++;
            }
          }
          ?>
            
          
                                        </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div><!-- /.box-body -->

                                <div class="box-footer clearfix">
                                <center> <?php echo $halaman; ?></center>
                                    <a href='<?php echo base_url();?>Admin_dashboard/formInsert' class="btn btn-sm btn-info btn-flat pull-right"><span class='glyphicon glyphicon-plus'></span>Tambah User</a>
                                    
                                </div><!-- /.box-footer -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->


 
        
      </div>
    </div>
  </div>
</div>











  <!-- you can finish add content here -->
 
  </div><!-- /.row -->

                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
                   
                                                                <?php
$this->load->view('template/footer');
?>                       
   
          
                                                                <?php
$this->load->view('template/footer2');
?>                       
   
        </body>
</html>