<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>CodeIgniter User Registration Form Demo</title>
	<link href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<?php echo $this->session->flashdata('verify_msg'); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>User Registration Form</h4>
			</div>
			<div class="panel-body">
				<?php $attributes = array("name" => "registrationform");
				echo form_open("Register/register", $attributes);?>
				<div class="form-group">
					<label for="name">First Name</label>
					<input class="form-control" name="fname" placeholder="Your First Name" type="text" value="<?php echo set_value('fname'); ?>" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

				<div class="form-group">
					<label for="name">Last Name</label>
					<input class="form-control" name="lname" placeholder="Last Name" type="text" value="<?php echo set_value('lname'); ?>" />
					<span class="text-danger"><?php echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="name">Jenis Kelamin</label>
					<input class="form-control" name="gender" placeholder="gender" type="text" value="<?php echo set_value('gender'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="name">Tangal Lahir</label>
					<input class="form-control" name="ttl" placeholder="ttl" type="text" value="<?php echo set_value('ttl'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
					<div class="form-group">
					<label for="name">No HP</label>
					<input class="form-control" name="contact" placeholder="No. HP" type="text" value="<?php echo set_value('contact'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				
					<div class="form-group">
					<label for="name">Alamat</label>
					<input class="form-control" name="alamat" placeholder="Alamat" type="text" value="<?php echo set_value('alamat'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
<!--  
					<div class="form-group">
					<label for="name">Provinsi</label>
					<input class="form-control" name="provinsi" placeholder="" type="text" value="<?php// echo set_value('provinsi'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="name">Kabupaten</label>
					<input class="form-control" name="kabupaten" placeholder="" type="text" value="<?php //echo set_value('kabupaten'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="name">Kecamatan</label>
					<input class="form-control" name="kecamatan" placeholder="" type="text" value="<?php //echo set_value('kecamatan'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>

-->
					<div class="form-group">
					<label for="name">Kode Pos</label>
					<input class="form-control" name="kode_pos" placeholder="" type="text" value="<?php echo set_value('kode_pos'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="email">Email ID</label>
					<input class="form-control" name="email" placeholder="Email-ID" type="text" value="<?php echo set_value('email'); ?>" />
					<span class="text-danger"><?php echo form_error('email'); ?></span>
				</div>
<div class="form-group">
					<label for="name">Username</label>
					<input class="form-control" name="username" placeholder="username" type="text" value="<?php echo set_value('username'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="subject">Password</label>
					<input class="form-control" name="password" placeholder="Password" type="password" />
					<span class="text-danger"><?php echo form_error('password'); ?></span>
				</div>

				<div class="form-group">
					<label for="subject">Confirm Password</label>
					<input class="form-control" name="cpassword" placeholder="Confirm Password" type="password" />
					<span class="text-danger"><?php echo form_error('cpassword'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-default">Signup</button>
					<button name="cancel" type="reset" class="btn btn-default">Cancel</button>
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>