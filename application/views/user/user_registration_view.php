<!DOCTYPE html>
<html>
<head>
	    <meta charset="UTF-8">
        <title>User Registration</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" >
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">  
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.min.css'); ?>" rel="stylesheet">        
        <!-- iCheck -->
        <link href="<?php echo base_url('assets/js/plugins/iCheck/square/blue.css'); ?>" rel="stylesheet">


         <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"></link>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/css/bootstrap-datetimepicker.min.css" rel="stylesheet"></link>
</head>
<body>
<div class="container">
<div class="row">
 
                            
                            	<?php echo $this->session->flashdata('verify_msg'); ?>
                         
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Registration Form</h4>
			</div>
			<div class="panel-body">
				<?php $attributes = array("name" => "registrationform");
				echo form_open("user/register/register", $attributes);?>
				<div class="form-group">
					<label for="name">First Name</label>
					<input class="form-control" name="fname" placeholder="Your First Name" type="text" value="<?php echo set_value('fname'); ?>" />
					<span class="text-danger"><?php echo form_error('fname'); ?></span>
				</div>

				<div class="form-group">
					<label for="name">Last Name</label>
					<input class="form-control" name="lname" placeholder="Last Name" type="text" value="<?php echo set_value('lname'); ?>" />
					<span class="text-danger"><?php echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
			
					<label for="name">Jenis Kelamin</label>
					<input class="form-control" name="gender" placeholder="gender" type="text" value="<?php echo set_value('gender'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<label for="name">Tangal Lahir</label>
				<div class="form-group">
					<div class='input-group date' id='datetimepicker'>
					
					<input class="form-control" name="ttl" placeholder="ttl" type="text" value="<?php echo set_value('ttl'); ?>" />
					<span class="input-group-addon">
   <span class="glyphicon glyphicon-calendar"></span>
  </span>
				</div></div>
					<div class="form-group">
					<label for="name">No HP</label>
					<input class="form-control" name="contact" placeholder="No. HP" type="text" value="<?php echo set_value('contact'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				
					<div class="form-group">
					<label for="name">Alamat</label>
					<input class="form-control" name="alamat" placeholder="Alamat" type="text" value="<?php echo set_value('alamat'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
<!--  
					<div class="form-group">
					<label for="name">Provinsi</label>
					<input class="form-control" name="provinsi" placeholder="" type="text" value="<?php// echo set_value('provinsi'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="name">Kabupaten</label>
					<input class="form-control" name="kabupaten" placeholder="" type="text" value="<?php //echo set_value('kabupaten'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="name">Kecamatan</label>
					<input class="form-control" name="kecamatan" placeholder="" type="text" value="<?php //echo set_value('kecamatan'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>

-->
					<div class="form-group">
					<label for="name">Kode Pos</label>
					<input class="form-control" name="kode_pos" placeholder="" type="text" value="<?php echo set_value('kode_pos'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="email">Email ID</label>
					<input class="form-control" name="email" placeholder="Email-ID" type="text" value="<?php echo set_value('email'); ?>" />
					<span class="text-danger"><?php echo form_error('email'); ?></span>
				</div>
<div class="form-group">
					<label for="name">Username</label>
					<input class="form-control" name="username" placeholder="username" type="text" value="<?php echo set_value('username'); ?>" />
					<span class="text-danger"><?php// echo form_error('lname'); ?></span>
				</div>
				<div class="form-group">
					<label for="subject">Password</label>
					<input class="form-control" name="password" placeholder="Password" type="password" />
					<span class="text-danger"><?php echo form_error('password'); ?></span>
				</div>

				<div class="form-group">
					<label for="subject">Confirm Password</label>
					<input class="form-control" name="cpassword" placeholder="Confirm Password" type="password" />
					<span class="text-danger"><?php echo form_error('cpassword'); ?></span>
				</div>

				<div class="form-group">
					<button name="submit" type="submit" class="btn btn-default">Signup</button>
					<button name="cancel" type="reset" class="btn btn-default">Cancel</button>
				</div>
				<?php echo form_close(); ?>
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
	</div>
</div>
</div>


 <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
 $(function () {
  $('#datetimepicker').datetimepicker({
   format: 'YYYY-MM-DD',
  });
  
  $('#datepicker').datetimepicker({
   format: 'YYYY-MM-DD',
  });
  
 
 });
</script>
</body>
</html>