/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50619
Source Host           : localhost:3306
Source Database       : db_mvc

Target Server Type    : MYSQL
Target Server Version : 50619
File Encoding         : 65001

Date: 2016-06-20 14:06:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `m_costumer`
-- ----------------------------
DROP TABLE IF EXISTS `m_costumer`;
CREATE TABLE `m_costumer` (
  `id_costumer` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) DEFAULT '',
  `hashpassword` varchar(255) NOT NULL,
  `password` varchar(40) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `born` date DEFAULT NULL,
  `sex` varchar(15) DEFAULT NULL,
  `contact` varchar(12) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kota_kab` varchar(20) DEFAULT NULL,
  `kecamatan` varchar(20) DEFAULT NULL,
  `address` varchar(40) DEFAULT NULL,
  `zip_code` varchar(7) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `hash` text,
  PRIMARY KEY (`id_costumer`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_costumer
-- ----------------------------
INSERT INTO `m_costumer` VALUES ('1', 'iim', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'iim', 'nurdiansyah', 'iimnurdiansyah@gmail.com', '2016-05-06', 'pria', '234', null, null, null, 'afs', '213', '1', '3ec9e9009707ac265004fc4a5c8ff902');

-- ----------------------------
-- Table structure for `tb_uploadimage`
-- ----------------------------
DROP TABLE IF EXISTS `tb_uploadimage`;
CREATE TABLE `tb_uploadimage` (
  `thumb` varchar(30) DEFAULT NULL,
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nm_gbr` varchar(35) DEFAULT NULL,
  `tipe_gbr` varchar(10) DEFAULT NULL,
  `ket_gbr` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_uploadimage
-- ----------------------------
INSERT INTO `tb_uploadimage` VALUES ('file_user_1462591672.jpg', '1', 'file_user_1462591672.jpg', 'image/jpeg', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `hash_password` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `role_id` int(2) NOT NULL,
  `role` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `create_user` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `image` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Iim Nur Diansyah', 'superuser', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', 'Super User', '2016-05-06 22:45:08', '2016-05-07 19:32:49', 'file_user_id_1.jpg');
INSERT INTO `user` VALUES ('2', 'rianti', 'rianti', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2', 'admin', '2016-05-06 22:48:20', null, null);
INSERT INTO `user` VALUES ('3', 'testing', 'testing', 'testing', 'ae2b1fca515949e5d54fb22b8ed95575', '1', 'Super User', '2016-05-07 11:15:36', null, 'file_user_1462594536');
INSERT INTO `user` VALUES ('4', 'testiing', 'testing', 'testing', 'ae2b1fca515949e5d54fb22b8ed95575', '2', 'admin', '2016-05-07 11:18:22', null, '');
INSERT INTO `user` VALUES ('5', 'superman', 'superman', 'superman', '84d961568a65073a3bcf0eb216b2a576', '1', 'Super User', '2016-05-07 11:20:33', null, '2016-05-07  11:20:33');
